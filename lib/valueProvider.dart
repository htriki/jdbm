import 'package:flutter/material.dart';

/**
 * Hocine Triki (2020)
 *
 */
class ValueProvider with ChangeNotifier {

  static double _MIN_VALUE = 500;

  int    _count = 0;
  double _value = 0;
  double _moyenne = 0;
  double _min = _MIN_VALUE;
  double _max = 0;
  var _t = [];

  double get value => _value;

  raz() {
    _count = 0;
    _value = 0;
    _moyenne = 0;
    _min = _MIN_VALUE;
    _max = 0;
    _t = [];
  }

  set value(double value) {

    _count++;
    _value = value;
    if (_value > _max) {
      _max = _value;
    }
    if (_value != 0 && _value < _min) {
      _min = _value;
    }

    _t.add(_value);
    var sum = _t.reduce((a, b) => a + b);
    _moyenne = sum / _count;

    notifyListeners();
  }

  double get max => _max;

  double get min {
    if (_min == _MIN_VALUE) {
      return 0;
    } else {
      return _min;
    }
  }

  double get moyenne => _moyenne;

  int get count => _count;
}