import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jdbm/info.dart';
import 'package:jdbm/valueProvider.dart';
import 'package:noise_meter/noise_meter.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

void main() {
  runApp(
    MyApp(),
  );
}

const title = "JdBm";

/**
 * Hocine Triki (2020)
 */
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ValueProvider>.value(
          value: ValueProvider(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(title: title),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  bool _isRecording = false;
  StreamSubscription<NoiseReading> _noiseSubscription;
  NoiseMeter _noiseMeter;

  IconData _icon = Icons.keyboard_voice;
  Color color = Colors.green;

  @override
  void initState() {
    super.initState();
    _noiseMeter = new NoiseMeter(onError);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }
  void onData(NoiseReading noiseReading) {

    double decibel = noiseReading.meanDecibel;
    var v = Provider.of<ValueProvider>(context, listen: false);
    v.value = decibel;

    /*
    this.setState(() {
      if (!this._isRecording) {
        this._isRecording = true;
      }
    });*/
  }

  void onError(PlatformException e) {
    print(e.toString());
    _isRecording = false;
  }

  void start() async {
    try {
      _noiseSubscription = _noiseMeter.noiseStream.listen(onData);
      this.setState(() {
        this._icon = Icons.stop;
        this.color = Colors.red;
        this._isRecording = true;
      });
    } catch (err) {
      print(err);
    }
  }

  void stop() async {
    try {
      if (_noiseSubscription != null) {
        _noiseSubscription.cancel();
        _noiseSubscription = null;
      }
      var valueProvider = Provider.of<ValueProvider>(context, listen: false);
      valueProvider.value = 0;

      this.setState(() {
        this._isRecording = false;
        this._icon = Icons.keyboard_voice;
        this._isRecording = false;
        this.color = Colors.green;
      });

    } catch (err) {
      print('stopRecorder error: $err');
    }
  }
  /*
  void onData(NoiseEvent e) {
    double decibel = e.decibel.toDouble();
    var v = Provider.of<ValueProvider>(context, listen: false);
    v.value = decibel;
  }

  void startRecorder() async {
    try {
      _noise = new Noise(500); // New observation every 500 ms
      _noiseSubscription = _noise.noiseStream.listen(onData);
      this.setState(() {
        this._icon = Icons.stop;
        this.color = Colors.red;
        this._isRecording = true;
      });
    } on NoiseMeterException catch (exception) {
      print(exception);
    }
  }

  void stopRecorder() async {
    try {
      if (_noiseSubscription != null) {
        _noiseSubscription.cancel();
        _noiseSubscription = null;
      }
      var valueProvider = Provider.of<ValueProvider>(context, listen: false);
      valueProvider.value = 0;

      this.setState(() {
        this._icon = Icons.keyboard_voice;
        this._isRecording = false;
        this.color = Colors.green;
      });
    } catch (err) {
      print('stopRecorder error: $err');
    }
  }*/

  void raz() {
    var valueProvider = Provider.of<ValueProvider>(context, listen: false);
    valueProvider.raz();
    this.setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    var value = Provider.of<ValueProvider>(context).value;
    var colorValue = Colors.green;
    if (value > 50) {
      if (value > 100) {
        colorValue = Colors.red;
      } else {
        colorValue = Colors.orange;
      }
    }
    var valueString = value.toStringAsFixed(0) + " dB";

    return Scaffold(
        appBar: AppBar(title: Text(this.widget.title)),
        backgroundColor: Colors.white,
        body: Column(children: <Widget>[
          SfRadialGauge(
              title: GaugeTitle(
                  text: 'Sonomètre',
                  textStyle: const TextStyle(
                      fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.pink)),
              axes: <RadialAxis>[
                RadialAxis(minimum: 0, maximum: 150, ranges: <GaugeRange>[
                  GaugeRange(
                      startValue: 0,
                      endValue: 50,
                      color: Colors.green,
                      startWidth: 10,
                      endWidth: 10),
                  GaugeRange(
                      startValue: 50,
                      endValue: 100,
                      color: Colors.orange,
                      startWidth: 10,
                      endWidth: 10),
                  GaugeRange(
                      startValue: 100,
                      endValue: 150,
                      color: Colors.red,
                      startWidth: 10,
                      endWidth: 10)
                ], pointers: <GaugePointer>[
                  NeedlePointer(value: value)
                ], annotations: <GaugeAnnotation>[
                  GaugeAnnotation(
                      widget: Container(
                          child: Text(valueString,
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold, color: colorValue))),
                      angle: 90,
                      positionFactor: 0.5)
                ])
              ]),
          /*Text(
            'Hocine...',
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),*/
          Info()
        ]),
        floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FloatingActionButton(
                child: Text("RAZ"),
                onPressed: () => {
                  this.raz()
                },
              ),
            ),
            FloatingActionButton(
              onPressed: () => {
                if (this._isRecording)
                  {this.stop()}
                else
                  {this.start()}
              },
              child: Icon(_icon),
              backgroundColor: color,
            )
          ],
        ));
    // This trailing comma makes auto-formatting nicer for build methods.
  }
}
