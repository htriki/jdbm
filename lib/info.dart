import 'package:flutter/material.dart';
import 'package:jdbm/valueProvider.dart';
import 'package:provider/provider.dart';

/**
 * Hocine Triki (2020)
 */
class Info extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    var valueProvider = Provider.of<ValueProvider>(context);
    var min = valueProvider.min.toStringAsFixed(0) + " dB";
    var moyenne = valueProvider.moyenne.toStringAsFixed(2) + " dB";
    var max = valueProvider.max.toStringAsFixed(0) + " dB";
    var count = valueProvider.count.toString();

    var ts = TextStyle(color: Colors.pink);
    var tsv = TextStyle(fontWeight: FontWeight.bold, color: Colors.pink);

    return Container(
      height: 56.0, // in logical pixels
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      //decoration: BoxDecoration(color: Colors.blue[500]),

      child: Row(
        // <Widget> is the type of items in the list.
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: [
              Text("MIN", style: ts),
              Text(
                min,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: tsv,
              ),
            ],
          ),
          Column(
            children: [
              Text("MOY", style: ts),
              Text(
                moyenne,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: tsv,
              ),
            ],
          ),
          Column(
            children: [
              Text("NBR", style: ts),
              Text(
                count,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: tsv,
              ),
            ],
          ),
          Column(
            children: [
              Text("MAX", style: ts),
              Text(
                max,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                style: tsv,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
